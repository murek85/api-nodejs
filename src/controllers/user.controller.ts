import {
  Body,
  Get,
  HttpError,
  JsonController,
  Post
} from "routing-controllers";
import { User } from "./../models/user/user";

@JsonController("/users")
export default class UserController {

  @Get("/info")
  public async getInfo() {
    const user = new User();
    user.email = "marcinmur@gmail.com";
    return user;
  }
}
