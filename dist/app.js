"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const dotenv_1 = __importDefault(require("dotenv"));
const errorhandler_1 = __importDefault(require("errorhandler"));
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
require("reflect-metadata");
const routing_controllers_1 = require("routing-controllers");
const user_controller_1 = __importDefault(require("./controllers/user.controller"));
const isProduction = process.env.NODE_ENV === "production";
// mongoose.Promise = global.Promise;
const app = express_1.default();
app.use(body_parser_1.default.json());
app.use(cors_1.default());
app.use(morgan_1.default("dev"));
if (!isProduction) {
    app.use(errorhandler_1.default());
    // mongoose.set("debug", true);
    dotenv_1.default.config();
}
const port = process.env.PORT || 8080; // default port to listen
// mongoose.set("useFindAndModify", false);
// mongoose.connect("mongodb://localhost:27017/", {useNewUrlParser: true});
routing_controllers_1.useExpressServer(app, {
    controllers: [
        user_controller_1.default
    ]
});
// start the Express server
const server = app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
exports.default = server;
//# sourceMappingURL=app.js.map